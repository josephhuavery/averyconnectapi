package com.avery.connect.AveryConnectApi

import com.sun.istack.internal.logging.Logger
import org.slf4j.LoggerFactory
import org.slf4j.event.Level
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import java.nio.charset.Charset
import java.util.*

data class DotcmsResult(val sku: String, val identifier: String?, val inode : String?, val location : String?)

interface DotcmsService {
    fun post(url : String, username : String, password : String, sku: String, input : String) : DotcmsResult
}

@Component
class DotcmsServiceImpl : DotcmsService {
    override fun post(url : String, username : String, password : String, sku: String, input: String) : DotcmsResult {

        val auth = "$username:$password"
        val encodedAuth = Base64.getEncoder().encode(
            auth.toByteArray(Charset.forName("US-ASCII"))
        )
        val authHeader = "Basic " + String(encodedAuth)

        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        headers.add("Authorization", authHeader)

        val entity = HttpEntity(input, headers)
        try {
            val result = RestTemplate().exchange(url, HttpMethod.POST, entity, String::class.java)

            val identifier = result.headers["identifier"]?.get(0)
            val inode = result.headers["inode"]?.get(0)
            val location = result.headers["Location"]?.get(0)

            return DotcmsResult(sku, identifier, inode, location)
        } catch (e: Exception) {
            //println("FAILED: post to $url resulted in ${e.message}")
            logger.error("FAILED: post to $url resulted in ${e.message}")
            return DotcmsResult(sku, null, null, null)
        }
    }

    companion object {
        val logger = LoggerFactory.getLogger(DotcmsServiceImpl::class.java)
    }
}