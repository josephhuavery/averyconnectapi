package com.avery.connect.AveryConnectApi

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.data.repository.CrudRepository
import java.io.StringWriter
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name="dotcms_medias_info")
data class MediaGroupInfo (@Id var sku : String, var identifier : String?, var inode: String?, var location : String?, var created_at : Date, var updated_at : Date) {
    // create a default constructor for Hibernate
    constructor() : this("", null, null, null, Date(), Date())
}

// Jackson Notes ************************************
// Jackson will always generate JSON with camelCase property names
// By default, multiple leading capital letters will all be converted to lower case.  E.g. SKU ==> sku
// This is unless you enable MapperFeature.USE_STD_BEAN_NAMING (added in Jackson 2.5), then it will follow Java Bean naming convention
// which is to only lower-case a single upper-case leading letter.  E.g. SKU ==> sKU

@JsonIgnoreProperties(ignoreUnknown = true)
data class MediaGroup (
    val SKU : String,
    val media_1 : String?,
    val media_2 : String?,
    val media_3 : String?,
    val media_4 : String?,
    val media_5 : String?,
    val media_6 : String?,
    val media_7 : String?,
    val media_8 : String?,
    val media_9 : String?
) {
    fun toDotCMS(identifier: String?) : DotcmsMediaGroup
            = DotcmsMediaGroup("ProductMedias", identifier, SKU, media_1, media_2, media_3, media_4, media_5, media_6, media_7, media_8, media_9)
}

// dotCMS content API requires 2 additional fields
// stName
// identifier (if null, then a new identifier (UUID) will be assigned
@JsonIgnoreProperties(ignoreUnknown = true)
// we do not want to generate {..., "identifier" : null, ...}
// we just want to have {..., ...} with ***"identifier" : null,*** taken out
// so we ask Jackson to include non-nulls only
@JsonInclude(JsonInclude.Include.NON_NULL)
data class DotcmsMediaGroup (
    val stName : String = "ProductMedias",
    var identifier : String?,
    // This does not work - see https://github.com/FasterXML/jackson-databind/issues/1609
    // @JsonProperty("SKU")
    val SKU : String,
    val media_1 : String?,
    val media_2 : String?,
    val media_3 : String?,
    val media_4 : String?,
    val media_5 : String?,
    val media_6 : String?,
    val media_7 : String?,
    val media_8 : String?,
    val media_9 : String?
) {
    fun json() : String {
        val strw = StringWriter()
        ObjectMapper().writeValue(strw,this)
        // Since @JsonProperty("SKU") does not work, we convert to upper case ourselves
        return strw.toString().replace("\"sku\"", "\"SKU\"")
    }
}

interface MediaGroupInfoRepository : CrudRepository<MediaGroupInfo, String>

