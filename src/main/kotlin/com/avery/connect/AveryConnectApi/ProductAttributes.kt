package com.avery.connect.AveryConnectApi

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.data.repository.CrudRepository
import java.io.StringWriter
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name="dotcms_attributes_info")
data class ProdAttrInfo (@Id var sku : String, var identifier : String?, var inode: String?, var location : String?, var created_at : Date, var updated_at : Date) {
    // create a default constructor for Hibernate
    constructor() : this("", null, null, null, Date(), Date())
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class ProdAttr (
    val SKU : String,
    val DisplayNameNotHTMLEncoded : String?,
    val DisplayName : String?,
    val AverycomCategory : String?,
    val AverycomSubcategory : String?,
    val ProductFamilyName : String?,
    val FlipcCatalogCopy : String?,
    val Feature1 : String?,
    val Feature2 : String?,
    val Feature3 : String?,
    val Feature4 : String?,
    val Feature5 : String?,
    val Feature6 : String?,
    val Feature7 : String?,
    val Feature8 : String?,
    val Feature9 : String?,
    val Feature10 : String?,
    val ConsumerCopyLongDescriptionHTMLEncoded : String?,
    val ConsumerCopyNotHTMLEncoded : String?,
    val CompleteDescriptionHTMLEncoded : String?,
    val CompleteDescriptionNotHTMLEncoded : String?,
    val Keywords : String?,
    val MetaTitle : String?,
    val MetaDescription : String?,
    val CrossSellSKU : String?,
    val UpSellSKU : String?
) {
    fun toDotCMS(identifier: String?) : DotcmsProdAttr
            = DotcmsProdAttr("ProductAttributes", identifier, SKU,
        DisplayNameNotHTMLEncoded,
        DisplayName,
        AverycomCategory,
        AverycomSubcategory,
        ProductFamilyName,
        FlipcCatalogCopy,
        Feature1,
        Feature2,
        Feature3,
        Feature4,
        Feature5,
        Feature6,
        Feature7,
        Feature8,
        Feature9,
        Feature10,
        ConsumerCopyLongDescriptionHTMLEncoded,
        ConsumerCopyNotHTMLEncoded,
        CompleteDescriptionHTMLEncoded,
        CompleteDescriptionNotHTMLEncoded,
        Keywords,
        MetaTitle,
        MetaDescription,
        CrossSellSKU,
        UpSellSKU)
}

// dotCMS content API requires 2 additional fields
// stName
// identifier (if null, then a new identifier (UUID) will be assigned
@JsonIgnoreProperties(ignoreUnknown = true)
// we do not want to generate {..., "identifier" : null, ...}
// we just want to have {..., ...} with ***"identifier" : null,*** taken out
// so we ask Jackson to include non-nulls only
@JsonInclude(JsonInclude.Include.NON_NULL)
data class DotcmsProdAttr (
    val stName : String = "ProductAttributes",
    var identifier : String?,
    // This does not work - see https://github.com/FasterXML/jackson-databind/issues/1609
    // @JsonProperty("SKU")
    val SKU : String,
    val DisplayNameNotHTMLEncoded : String?,
    val DisplayName : String?,
    val AverycomCategory : String?,
    val AverycomSubcategory : String?,
    val ProductFamilyName : String?,
    val FlipcCatalogCopy : String?,
    val Feature1 : String?,
    val Feature2 : String?,
    val Feature3 : String?,
    val Feature4 : String?,
    val Feature5 : String?,
    val Feature6 : String?,
    val Feature7 : String?,
    val Feature8 : String?,
    val Feature9 : String?,
    val Feature10 : String?,
    val ConsumerCopyLongDescriptionHTMLEncoded : String?,
    val ConsumerCopyNotHTMLEncoded : String?,
    val CompleteDescriptionHTMLEncoded : String?,
    val CompleteDescriptionNotHTMLEncoded : String?,
    val Keywords : String?,
    val MetaTitle : String?,
    val MetaDescription : String?,
    val CrossSellSKU : String?,
    val UpSellSKU : String?
) {
    fun json() : String {
        val strw = StringWriter()
        ObjectMapper().writeValue(strw, this)
        // Since @JsonProperty("SKU") does not work, we convert to upper case ourselves
        return strw.toString()
            .replace("\"sku\"", "\"SKU\"")
            .replace("\"displayNameNotHTMLEncoded\"", "\"DisplayNameNotHTMLEncoded\"")
            .replace("\"displayName\"", "\"DisplayName\"")
            .replace("\"averycomCategory\"", "\"AverycomCategory\"")
            .replace("\"averycomSubcategory\"", "\"AverycomSubcategory\"")
            .replace("\"productFamilyName\"", "\"ProductFamilyName\"")
            .replace("\"flipcCatalogCopy\"", "\"FlipcCatalogCopy\"")
            .replace("\"feature1\"", "\"Feature1\"")
            .replace("\"feature2\"", "\"Feature2\"")
            .replace("\"feature3\"", "\"Feature3\"")
            .replace("\"feature4\"", "\"Feature4\"")
            .replace("\"feature5\"", "\"Feature5\"")
            .replace("\"feature6\"", "\"Feature6\"")
            .replace("\"feature7\"", "\"Feature7\"")
            .replace("\"feature8\"", "\"Feature8\"")
            .replace("\"feature9\"", "\"Feature9\"")
            .replace("\"feature10\"", "\"Feature10\"")
            .replace("\"consumerCopyLongDescriptionHTMLEncoded\"", "\"ConsumerCopyLongDescriptionHTMLEncoded\"")
            .replace("\"consumerCopyNotHTMLEncoded\"", "\"ConsumerCopyNotHTMLEncoded\"")
            .replace("\"completeDescriptionHTMLEncoded\"", "\"CompleteDescriptionHTMLEncoded\"")
            .replace("\"completeDescriptionNotHTMLEncoded\"", "\"CompleteDescriptionNotHTMLEncoded\"")
            .replace("\"keywords\"", "\"Keywords\"")
            .replace("\"metaTitle\"", "\"MetaTitle\"")
            .replace("\"metaDescription\"", "\"MetaDescription\"")
            .replace("\"crossSellSKU\"", "\"CrossSellSKU\"")
            .replace("\"upSellSKU\"", "\"UpSellSKU\"")
    }
}

interface ProdAttrInfoRepository : CrudRepository<ProdAttrInfo, String>

