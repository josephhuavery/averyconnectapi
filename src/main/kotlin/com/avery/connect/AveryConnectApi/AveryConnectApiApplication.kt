package com.avery.connect.AveryConnectApi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AveryConnectApiApplication

fun main(args: Array<String>) {
    runApplication<AveryConnectApiApplication>(*args)
}
