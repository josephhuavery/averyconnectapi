package com.avery.connect.AveryConnectApi

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.*
import org.springframework.web.bind.annotation.*
import org.springframework.web.bind.annotation.RestController
import java.util.Date

@RestController
class RestController() {

    @Value("\${dotcms.url.save}")
    lateinit var saveUrl : String

    @Value("\${dotcms.credential.username}")
    lateinit var username : String

    @Value("\${dotcms.credential.password}")
    lateinit var password : String

    @Autowired
    private
    lateinit var mediaRepo : MediaGroupInfoRepository

    @Autowired
    private
    lateinit var attrRepo : ProdAttrInfoRepository

    @Autowired
    lateinit var dotcmsService : DotcmsService

    @RequestMapping("/productMedias", method = arrayOf(RequestMethod.POST, RequestMethod.PUT))
    fun addMediaGroup(@RequestBody mg: MediaGroup) : ResponseEntity<String> {

        val sku = mg.SKU
        val mgInfo = mediaRepo.findById(sku)
        val identifier = if (mgInfo.isPresent) mgInfo.get().identifier else null

        val result = dotcmsService.post(saveUrl, username, password, mg.SKU, mg.toDotCMS(identifier).json())

        if (identifier != null) {
            val mgInfoToSave = mgInfo.get()
            // only update the updated_at column
            mgInfoToSave.updated_at = Date()
            mediaRepo.save(mgInfoToSave)
        } else if (result.identifier != null) {
            mediaRepo.save(MediaGroupInfo(result.sku, result.identifier, result.inode, result.location, Date(), Date()))
        }

        val headers = HttpHeaders()
        headers.add("SKU", result.sku)
        headers.add("identifier", result.identifier)
        headers.add("inode", result.inode)
        headers.add("Location", result.location)
        val response = ResponseEntity<String>("", headers, HttpStatus.OK)

        return response
    }

    @RequestMapping("/productAttributes", method = arrayOf(RequestMethod.POST, RequestMethod.PUT))
    fun addProdAttr(@RequestBody attr: ProdAttr) : ResponseEntity<String> {

        val sku = attr.SKU
        val attrInfo = attrRepo.findById(sku)
        val identifier = if (attrInfo.isPresent) attrInfo.get().identifier else null

        val result = dotcmsService.post(saveUrl, username, password, attr.SKU, attr.toDotCMS(identifier).json())

        if (identifier != null) {
            val attrInfoToSave = attrInfo.get()
            // only update the updated_at column
            attrInfoToSave.updated_at = Date()
            attrRepo.save(attrInfoToSave)
        } else if (result.identifier != null) {
            attrRepo.save(ProdAttrInfo(result.sku, result.identifier, result.inode, result.location, Date(), Date()))
        }

        val headers = HttpHeaders()
        headers.add("SKU", result.sku)
        headers.add("identifier", result.identifier)
        headers.add("inode", result.inode)
        headers.add("Location", result.location)
        val response = ResponseEntity<String>("", headers, HttpStatus.OK)

        return response
    }

}